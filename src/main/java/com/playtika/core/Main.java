package com.playtika.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.telegram.telegrambots.TelegramApiException;
import org.telegram.telegrambots.TelegramBotsApi;


@SpringBootApplication
public class Main {
    public static void main(String[] args) throws TelegramApiException {
        ConfigurableApplicationContext ctx = SpringApplication.run(Main.class, args);

        Bot bot = ctx.getBean(Bot.class);

        new TelegramBotsApi().registerBot(bot);
    }

}
