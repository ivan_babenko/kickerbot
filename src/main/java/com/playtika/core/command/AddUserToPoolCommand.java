package com.playtika.core.command;

import com.playtika.core.UserPool;
import com.playtika.core.dto.TeamsHistoryDto;
import com.playtika.core.repositories.TeamsHistoryRepository;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.User;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import static com.google.common.collect.ImmutableMap.of;
import static com.playtika.core.Utils.getTeamNames;

@Component
public class AddUserToPoolCommand implements Command {

    public static final Pattern handlePattern = Pattern.compile("(\\+)|(/add)|(/add@KarinochkaBot)");
    @Autowired
    TeamsHistoryRepository teamsHistoryRepository;
    @Autowired
    private UserPool userPool;

    @Override
    public Pattern getHandlePattern() {
        return handlePattern;
    }


    @Override
    public String commandHelp() {
        return "`/add (+)`  _заявка на игру_";
    }

    @Override
    public String apply(Message message) {
        synchronized (userPool) {
            userPool.addUserToPool(
                    message.getFrom());
            if (userPool.isFullPool()) {
                return startTheGame();
            }
        }
        return null;
    }

    private String startTheGame() {
        List<User> teamA = new ArrayList<>();
        List<User> teamB = new ArrayList<>();
        userPool.splitPool(teamA, teamB);
        userPool.clearPool();

        TeamsHistoryDto event = teamsHistoryRepository.save(new TeamsHistoryDto(
                teamA.get(0).getId(),
                teamA.get(1).getId(),
                teamB.get(0).getId(),
                teamB.get(1).getId()));

        return "Играют:\n*"
                + getTeamNames(teamA)
                + "\n vs \n"
                + getTeamNames(teamB)
                + "*\n" + "Всем писюнов!"
                + "\nномер матча #" + event.getMatchId();
    }
}

