package com.playtika.core.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.objects.Message;

import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
public class HelpCommand implements Command {

    public static final Pattern handlePattern = Pattern.compile("(/help)|(/help@KarinochkaBot)");
    private Set<Command> commands;

    @Autowired
    public HelpCommand(Set<Command> commands) {
        this.commands = commands;
        this.commands.add(this);
    }

    @Override
    public Pattern getHandlePattern() {
        return handlePattern;
    }

    @Override
    public String commandHelp() {
        return "`/help`  _помощь_";
    }

    @Override
    public String apply(Message message) {
        return "Доступные команды:\n" + commands.stream()
                .map(Command::commandHelp)
                .filter(h -> h != null)
                .collect(Collectors.joining("\n"));
    }
}
