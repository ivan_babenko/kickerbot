package com.playtika.core.command;

import com.google.common.base.Strings;
import com.playtika.core.Utils;
import com.playtika.core.dto.UserDto;
import com.playtika.core.dto.UserStatsDto;
import com.playtika.core.repositories.UserRepository;
import com.playtika.core.repositories.UserStatsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.objects.Message;

import java.util.regex.Pattern;

@Component
public class MeCommand implements Command {

    public static final Pattern handlePattern = Pattern.compile("(/me(@KarinochkaBot)*( (\\d+))*)");
    public static final int DAYS_TO_COUNT_GROUP_IN_PATTERN = 4;

    @Value("${pisyun.expirationTime}")
    private int pisyunExpirationTime;
    @Value("${stats.windowDays}")
    private int windowDays;
    @Autowired
    private UserStatsRepository userStatsRepository;
    @Autowired
    private UserRepository userRepository;

    @Override
    public String apply(Message message) {
        UserDto user = userRepository.findOne(message.getFrom().getId());
        UserStatsDto me = userStatsRepository.getMe(message.getFrom().getId(), System.currentTimeMillis() - pisyunExpirationTime, System.currentTimeMillis() - Utils.getDaysInMilliseconds(handlePattern.matcher(message.getText()),
                windowDays, DAYS_TO_COUNT_GROUP_IN_PATTERN));


        if (me == null) {
            return String.format("%s, так ты ж еще и матча не сыграл! Быстро ставь _+_!", Utils.makeName(message.getFrom()));
        } else {

            StringBuilder result = new StringBuilder("`");

            if (me.isPisyun()) {
                result.append("Поздравляем, ")
                        .append(Utils.makeName(user))
                        .append(", ты - ПИСЮН! :)")
                        .append("\nКонец мучениям: ")
                        .append(Utils.formatExpirationTime(me.getPisyunStartTime() + pisyunExpirationTime));

            }

            result.append("\nПобед:")
                    .append(me.getTotalWins())
                    .append("\nПоражений:")
                    .append(me.getTotalLosses())
                    .append("\nБалл:")
                    .append(1.0 * me.getTotalWins() / (me.getTotalWins() + me.getTotalLosses()));
            result.append("`");

            String response = result.toString();
            return Strings.isNullOrEmpty(response) ? null : response;
        }
    }

    @Override
    public Pattern getHandlePattern() {
        return handlePattern;
    }

    @Override
    public String commandHelp() {
        return "`/me`  _отобразить свою статистику_";
    }
}
