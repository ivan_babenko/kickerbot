package com.playtika.core.command;

import com.playtika.core.UserPool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.objects.Message;

import java.util.regex.Pattern;

@Component
public class RemoveUserFromPoolCommand implements Command {

    public static final Pattern handlePattern = Pattern.compile("(-)|(/remove)|(/remove@KarinochkaBot)");
    @Autowired
    private UserPool userPool;

    @Override
    public Pattern getHandlePattern() {
        return handlePattern;
    }

    @Override
    public String commandHelp() {
        return "`/remove (-)`  _удалить заявку на игру_";
    }

    @Override
    public String apply(Message message) {
        userPool.removeUserFromPool(
                message.getFrom());
        return null;
    }
}
