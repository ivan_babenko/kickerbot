package com.playtika.core.command;

import org.telegram.telegrambots.api.objects.Message;

import java.util.function.Function;
import java.util.regex.Pattern;

public interface Command extends Function<Message, String> {

    String commandHelp();

    Pattern getHandlePattern();

    default boolean isHandles(String message) {
        return getHandlePattern().matcher(message).matches();
    }
}
