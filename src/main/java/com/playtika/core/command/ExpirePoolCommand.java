package com.playtika.core.command;

import com.playtika.core.UserPool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.objects.Message;

import java.util.regex.Pattern;

@Component
public class ExpirePoolCommand implements Command {

    public static final Pattern handlePattern = Pattern.compile("(/expire)|(/expire@KarinochkaBot)");

    @Autowired
    private UserPool userPool;

    @Override
    public Pattern getHandlePattern() {
        return handlePattern;
    }

    @Override
    public String commandHelp() {
        return "`/expire`  _время до конца заявок_";
    }

    @Override
    public String apply(Message message) {
        if (!userPool.isPoolExpired() && userPool.getPool().size() > 0) {
            return new StringBuffer()
                    .append("Время до конца принятия заявок:\n")
                    .append(userPool.getExpiration())
                    .append("\n")
                    .append(userPool.getPool().size() > 0 ? userPool.toString() : "")
                    .toString();
        } else {
            return "Нет активных пулов.";
        }
    }
}
