package com.playtika.core.command;

import com.playtika.core.Utils;
import com.playtika.core.dto.UserDto;
import com.playtika.core.repositories.PisyunRepository;
import com.playtika.core.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.objects.Message;

import java.util.List;
import java.util.regex.Pattern;

import static java.util.stream.Collectors.toList;

@Component
public class NotifyCommand implements Command {

    public static final Pattern handlePattern = Pattern.compile(".*");
    @Value("${pisyun.expirationTime}")
    private int pisyunExpirationTime;
    @Autowired
    private PisyunRepository pisyunRepository;
    @Autowired
    private UserRepository userRepository;

    @Override
    public Pattern getHandlePattern() {
        return handlePattern;
    }

    @Override
    public String commandHelp() {
        return null;
    }

    @Override
    public String apply(Message message) {
        String text = message.getText().toLowerCase();
        List<UserDto> mentionedPisyuns = pisyunRepository
                .findByTsGreaterThan(System.currentTimeMillis() - pisyunExpirationTime)
                .stream()
                .filter(p -> {
                    UserDto pisyun = userRepository.findOne(p.getUserId());
                    String name = Utils.makeName(pisyun).toLowerCase();

                    return text.contains(name);
                })
                .map(p -> userRepository.findOne(p.getUserId()))
                .collect(toList());

        if (mentionedPisyuns.size() == 1) {
            return "Писюн!";
        }

        if (mentionedPisyuns.size() > 1) {
            return "Писюны!";
        }

        return null;
    }
}
