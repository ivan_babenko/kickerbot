package com.playtika.core.command;

import com.google.common.base.Strings;
import com.playtika.core.Utils;
import com.playtika.core.dto.PisyunDto;
import com.playtika.core.dto.UserDto;
import com.playtika.core.repositories.PisyunRepository;
import com.playtika.core.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.objects.Message;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
public class PisyunsCommand implements Command {

    public static final Pattern handlePattern = Pattern.compile("(/pisyun)|(/pisyun@KarinochkaBot)");
    @Value("${pisyun.expirationTime}")
    private int pisyunExpirationTime;
    @Autowired
    private PisyunRepository pisyunRepository;
    @Autowired
    private UserRepository userRepository;
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    @Override
    public String apply(Message message) {
        List<PisyunDto> pisyuns = pisyunRepository
                .findByTsGreaterThan(System.currentTimeMillis() - pisyunExpirationTime);
        Map<Integer, PisyunDto> recentPisyuns = new HashMap<>();
        pisyuns.stream().forEach(p -> {
            PisyunDto pisyun = recentPisyuns.get(p.getUserId());
            if (pisyun != null && pisyun.getTs() < p.getTs()) {
                recentPisyuns.put(p.getUserId(), p);
            } else if (pisyun == null) {
                recentPisyuns.put(p.getUserId(), p);
            }
        });

        String result = recentPisyuns
                .values()
                .stream()
                .map(p -> {
                    UserDto pisyun = userRepository.findOne(p.getUserId());
                    return Utils.makeName(pisyun) + " до " + formatter.format(LocalDateTime.ofInstant(
                            Instant.ofEpochMilli(p.getTs() + pisyunExpirationTime),
                            TimeZone.getDefault().toZoneId()));
                })
                .collect(Collectors.joining("\n"));


        return Strings.isNullOrEmpty(result) ? "Нету писюнов" : result;
    }

    @Override
    public Pattern getHandlePattern() {
        return handlePattern;
    }

    @Override
    public String commandHelp() {
        return "`/pisyun`  _отобразить текущих писюнов_";
    }
}
