package com.playtika.core.command;

import com.google.common.base.Strings;
import com.playtika.core.Utils;
import com.playtika.core.dto.UserDto;
import com.playtika.core.dto.UserStatsDto;
import com.playtika.core.repositories.UserRepository;
import com.playtika.core.repositories.UserStatsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.objects.Message;

import java.util.List;
import java.util.regex.Pattern;

@Component
public class AllCommand implements Command {

    public static final Pattern handlePattern = Pattern.compile("(/all(@KarinochkaBot)*( (\\d+))*)");
    public static final int DAYS_TO_COUNT_GROUP_IN_PATTERN = 4;
    @Value("${pisyun.expirationTime}")
    private int pisyunExpirationTime;
    @Value("${stats.windowDays}")
    private int windowDays;
    @Autowired
    private UserStatsRepository userStatsRepository;
    @Autowired
    private UserRepository userRepository;

    @Override
    public String apply(Message message) {

        List<UserStatsDto> all = userStatsRepository.getAll(System.currentTimeMillis() - pisyunExpirationTime,
                System.currentTimeMillis() - Utils.getDaysInMilliseconds(handlePattern.matcher(message.getText()),
                        windowDays, DAYS_TO_COUNT_GROUP_IN_PATTERN));

        StringBuilder result = new StringBuilder();
        String header = "`%-20s%-10s%-10s%-10s%-5s\n`";
        String body = "`%-20s%-10s%-10s%-10s%5.3f\n`";
        if (all.size() > 0) {
            result.append(String.format(header, "Имя", "Писюн", "Побед", "Поражений", "Балл"));
            all.stream().forEach(it -> {
                        UserDto user = userRepository.findOne(it.getUserId());
                        result.append(String.format(body,
                                Utils.makeName(user),
                                (it.isPisyun()) ? "ПИСЮН" : "",
                                it.getTotalWins(),
                                it.getTotalLosses(),
                                1.0 * (it.getTotalWins()) / (it.getTotalWins() + it.getTotalLosses())));
                    }
            );
        } else {
            return "Нима статистики, быстро ставьте _+_!";
        }

        String response = result.toString();
        return Strings.isNullOrEmpty(response) ? null : response;
    }

    @Override
    public Pattern getHandlePattern() {
        return handlePattern;
    }

    @Override
    public String commandHelp() {
        return "`/all`  _отобразить статистику для всех_";
    }
}
