package com.playtika.core.command;

import com.playtika.core.Utils;
import com.playtika.core.dto.PisyunDto;
import com.playtika.core.dto.ScoreDto;
import com.playtika.core.dto.TeamsHistoryDto;
import com.playtika.core.repositories.PisyunRepository;
import com.playtika.core.repositories.ScoreRepository;
import com.playtika.core.repositories.TeamsHistoryRepository;
import com.playtika.core.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.objects.Message;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class AddScoresCommand implements Command {

    public static final Pattern handlePattern = Pattern.compile("/score (\\d+)(( \\d+:\\d+)+)");
    public static final int DEFAULT_TIMEOUT_FOR_SCORES = 3600_000;

    private final Pattern scoreFinder = Pattern.compile("((\\d+):(\\d+))");

    @Autowired
    PisyunRepository pisyunRepository;
    @Autowired
    private ScoreRepository scoreRepository;
    @Autowired
    private TeamsHistoryRepository teamsHistoryRepository;
    @Autowired
    private UserRepository userRepository;

    @Override
    public String commandHelp() {
        return "`/score` _matchID A:B ... A:B ввод счета за конктертный матч_";
    }

    @Override
    public Pattern getHandlePattern() {
        return handlePattern;
    }

    @Override
    public String apply(Message message) {
        List<ScoreDto> scores = parseScores(message.getText());
        scores
                .stream()
                .filter(dto -> {
                    TeamsHistoryDto match = teamsHistoryRepository.findOne(dto.getMatchId());
                    return match == null ? false : match.getTs() > System.currentTimeMillis() - DEFAULT_TIMEOUT_FOR_SCORES;
                })
                .forEach(scoreRepository::save);

        List<PisyunDto> pisyuns = new ArrayList<>();
        scores
                .stream()
                .forEach(dto -> {
                    TeamsHistoryDto match = teamsHistoryRepository.findOne(dto.getMatchId());
                    if (dto.getA() == 0) {
                        pisyuns.add(new PisyunDto(match.getMatchId(), match.getTs(), match.getPlayerA1()));
                        pisyuns.add(new PisyunDto(match.getMatchId(), match.getTs(), match.getPlayerA2()));
                    } else if (dto.getB() == 0) {
                        pisyuns.add(new PisyunDto(match.getMatchId(), match.getTs(), match.getPlayerB1()));
                        pisyuns.add(new PisyunDto(match.getMatchId(), match.getTs(), match.getPlayerB2()));
                    }
                });
        pisyunRepository.save(pisyuns);

        StringBuilder sb = new StringBuilder()
                .append("Сохранено ")
                .append(scores.size())
                .append(" шт.");

        if (pisyuns.size() > 0) {
            sb.append("\nНовые писюны:\n");
            pisyuns
                    .stream()
                    .forEach(p -> sb
                            .append(Utils.makeName(userRepository.findOne(p.getUserId())))
                            .append("\n"));

        }
        return sb.toString();
    }

    public List<ScoreDto> parseScores(String command) {
        List<ScoreDto> result = new ArrayList<>();
        Matcher segmentFinder = handlePattern.matcher(command);
        if (segmentFinder.find()) {
            int matchId = Integer.valueOf(segmentFinder.group(1));
            String scoresString = segmentFinder.group(2);

            Matcher scores = scoreFinder.matcher(scoresString);
            while (scores.find()) {
                result.add(new ScoreDto(matchId,
                        Integer.valueOf(scores.group(2)),
                        Integer.valueOf(scores.group(3))));
            }
        }
        return result;

    }
}