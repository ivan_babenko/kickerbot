package com.playtika.core.repositories;

import com.playtika.core.dto.PisyunDto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PisyunRepository extends CrudRepository<PisyunDto, Integer> {

    List<PisyunDto> findByTsGreaterThan(long ts);
}
