package com.playtika.core.repositories;


import com.playtika.core.dto.TeamsHistoryDto;
import com.playtika.core.dto.UserDto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeamsHistoryRepository extends CrudRepository<TeamsHistoryDto, Integer> {
}
