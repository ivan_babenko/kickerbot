package com.playtika.core.repositories;


import com.playtika.core.dto.ScoreDto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScoreRepository extends CrudRepository<ScoreDto, Integer> {
}
