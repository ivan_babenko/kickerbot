package com.playtika.core.repositories;

import com.playtika.core.dto.UserStatsDto;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserStatsRepository extends CrudRepository<UserStatsDto, Integer> {

    @Query(value = "SELECT\n" +
            "  stats.id          AS user_id,\n" +
            "  CASE WHEN pisyuns.ts IS NULL\n" +
            "    THEN 0\n" +
            "  ELSE 1 END           is_pisyun,\n" +
            "  nvl(pisyuns.ts,0)        AS pisyun_start_time,\n" +
            "  stats.totalWins   AS total_wins,\n" +
            "  stats.totalLosses AS total_losses\n" +
            "FROM (SELECT\n" +
            "        id,\n" +
            "        sum(wins)   totalWins,\n" +
            "        sum(losses) totalLosses\n" +
            "      FROM (\n" +
            "        SELECT\n" +
            "          u.id,\n" +
            "          CASE WHEN (s.a > s.b AND u.ID IN (th.PLAYERA1, th.PLAYERA2))\n" +
            "                    OR (s.a < s.b AND u.ID IN (th.PLAYERB1, th.PLAYERB2))\n" +
            "            THEN 1\n" +
            "          ELSE 0 END wins,\n" +
            "          CASE WHEN (s.a < s.b AND u.ID IN (th.PLAYERA1, th.PLAYERA2))\n" +
            "                    OR (s.a > s.b AND u.ID IN (th.PLAYERB1, th.PLAYERB2))\n" +
            "            THEN 1\n" +
            "          ELSE 0 END losses\n" +
            "        FROM TEAMS_HISTORY_DTO th, SCORE_DTO s, USER_DTO u\n" +
            "        WHERE th.MATCH_ID = s.MATCH_ID\n" +
            "              AND (u.id IN (th.PLAYERA1, th.PLAYERA2) OR u.id IN (th.PLAYERB1, th.PLAYERB2))\n" +
            "              AND th.ts > :oldestTs\n" +
            "      )\n" +
            "      GROUP BY id) stats\n" +
            "  LEFT JOIN\n" +
            "  (SELECT\n" +
            "     user_id,\n" +
            "     max(ts) AS ts\n" +
            "   FROM PISYUN_DTO p\n" +
            "     where p.TS > :ts\n" +
            "   GROUP BY user_id) pisyuns\n" +
            "    ON stats.id = pisyuns.USER_ID" +
            " ORDER BY 1.0 * stats.totalWins / (stats.totalWins + stats.totalLosses) desc, stats.totalWins desc, stats.totalLosses asc", nativeQuery = true)
    List<UserStatsDto> getAll(@Param("ts") long pisyunTs, @Param("oldestTs") long oldestTs);

    @Query(value = "SELECT\n" +
            "  stats.id          AS user_id,\n" +
            "  CASE WHEN pisyuns.ts IS NULL\n" +
            "    THEN 0\n" +
            "  ELSE 1 END           is_pisyun,\n" +
            "  nvl(pisyuns.ts,0)        AS pisyun_start_time,\n" +
            "  stats.totalWins   AS total_wins,\n" +
            "  stats.totalLosses AS total_losses\n" +
            "FROM (SELECT\n" +
            "        id,\n" +
            "        sum(wins)   totalWins,\n" +
            "        sum(losses) totalLosses\n" +
            "      FROM (\n" +
            "        SELECT\n" +
            "          u.id,\n" +
            "          CASE WHEN (s.a > s.b AND u.ID IN (th.PLAYERA1, th.PLAYERA2))\n" +
            "                    OR (s.a < s.b AND u.ID IN (th.PLAYERB1, th.PLAYERB2))\n" +
            "            THEN 1\n" +
            "          ELSE 0 END wins,\n" +
            "          CASE WHEN (s.a < s.b AND u.ID IN (th.PLAYERA1, th.PLAYERA2))\n" +
            "                    OR (s.a > s.b AND u.ID IN (th.PLAYERB1, th.PLAYERB2))\n" +
            "            THEN 1\n" +
            "          ELSE 0 END losses\n" +
            "        FROM TEAMS_HISTORY_DTO th, SCORE_DTO s, USER_DTO u\n" +
            "        WHERE th.MATCH_ID = s.MATCH_ID\n" +
            "              AND (u.id IN (th.PLAYERA1, th.PLAYERA2) OR u.id IN (th.PLAYERB1, th.PLAYERB2))\n" +
            "              AND th.ts > :oldestTs\n" +
            "      )\n" +
            "      GROUP BY id) stats\n" +
            "  LEFT JOIN\n" +
            "  (SELECT\n" +
            "     user_id,\n" +
            "     max(ts) AS ts\n" +
            "   FROM PISYUN_DTO p\n" +
            "     where p.TS > :ts\n" +
            "   GROUP BY user_id) pisyuns\n" +
            "    ON stats.id = pisyuns.USER_ID\n" +
            "where stats.ID = :user_id", nativeQuery = true)
    UserStatsDto getMe(@Param("user_id") int userId, @Param("ts") long pisyunTs, @Param("oldestTs") long oldestTs);

}
