package com.playtika.core;

import com.google.common.collect.ImmutableMap;
import com.playtika.core.command.Command;
import com.playtika.core.dto.UserDto;
import com.playtika.core.repositories.UserRepository;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.TelegramApiException;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.User;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;

import java.util.Set;


@Component
public class Bot extends TelegramLongPollingBot {

    @Autowired
    UserRepository repository;

    @Autowired
    private Set<Command> commands;
    @Value("${botToken}")
    private String botToken;
    @Value("${botName}")
    private String botName;

    public String getBotUsername() {
        return botName;
    }

    public String getBotToken() {
        return botToken;
    }

    public void onUpdateReceived(Update update) {
        commands.stream()
                .filter(c -> c.isHandles(update.getMessage().getText()))
                .forEach(c -> {
                    String reply = c.apply(update.getMessage());
                    if (reply != null) {
                        sendMessage(reply, update.getMessage().getChatId());
                    }
                });
    }

    private void sendMessage(String messageText, long chatId) {
        SendMessage message = new SendMessage();
        message.setText(messageText).setChatId(String.valueOf(chatId)).enableMarkdown(true);
        try {
            sendMessage(message);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}
