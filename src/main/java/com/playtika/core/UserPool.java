package com.playtika.core;

import com.playtika.core.dto.UserDto;
import com.playtika.core.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.objects.User;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
public class UserPool {

    public static final int POOL_SIZE = 4;
    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm:ss");
    public static long POOL_REQUEST_TIMEOUT = TimeUnit.MINUTES.toMillis(15); //15 minutes

    @Autowired
    UserRepository userRepository;

    private Map<Integer, User> teamPool = new HashMap<>();
    private long lastPoolUpdate;

    public void addUserToPool(User user) {
        userRepository.save(UserDto.fromUser(user));

        if (isPoolExpired() || teamPool.size() == 0) {
            teamPool.clear();
        }
        if (!isFullPool() && !teamPool.containsKey(user.getId())) {
            teamPool.put(user.getId(), user);
            lastPoolUpdate = System.currentTimeMillis();
        }
    }

    public boolean isFullPool() {
        return teamPool.size() == POOL_SIZE;
    }

    public void removeUserFromPool(User user) {
        teamPool.remove(user.getId());
    }

    public Map<Integer, User> getPool() {
        return new HashMap<>(teamPool);
    }

    public void clearPool() {
        teamPool.clear();
    }

    public boolean isPoolExpired() {
        return teamPool.size() > 0 && (System.currentTimeMillis() - lastPoolUpdate > POOL_REQUEST_TIMEOUT);
    }

    public String getExpiration() {
        String message = "Активного пула нет!";
        if (teamPool.size() > 0 && !isPoolExpired()) {
            message = LocalDateTime.ofInstant(Instant.ofEpochMilli(lastPoolUpdate + POOL_REQUEST_TIMEOUT), ZoneId.systemDefault()).format(DATE_TIME_FORMATTER);
        }
        return message;
    }

    public void splitPool(Collection<User> teamA, Collection<User> teamB) {
        List<User> players = teamPool
                .values()
                .stream()
                .sorted((userA, userB) -> ThreadLocalRandom.current().nextInt() % 2 == 0 ? 1 : -1)
                .collect(Collectors.toList());
        teamA.addAll(players.subList(0, players.size() / 2));
        teamB.addAll(players.subList(players.size() / 2, players.size()));
    }

    @Override
    public String toString() {
        String pool = teamPool.values().stream()
                .map(Utils::makeName)
                .collect(Collectors.joining("\n"));
        return teamPool.size() == 0 ? "Пул пуст" : "Текущий пул:\n" + pool;
    }

}
