package com.playtika.core.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class ScoreDto {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private int matchId;
    private int A;
    private int B;

    public ScoreDto() {
    }

    public ScoreDto(int matchId, int a, int b) {
        this.matchId = matchId;
        A = a;
        B = b;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMatchId() {
        return matchId;
    }

    public void setMatchId(int matchId) {
        this.matchId = matchId;
    }

    public int getA() {
        return A;
    }

    public void setA(int a) {
        A = a;
    }

    public int getB() {
        return B;
    }

    public void setB(int b) {
        B = b;
    }

    @Override
    public String toString() {
        return "ScoreDto{" +
                "id=" + id +
                ", matchId=" + matchId +
                ", A=" + A +
                ", B=" + B +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ScoreDto scoreDto = (ScoreDto) o;
        return id == scoreDto.id &&
                matchId == scoreDto.matchId &&
                A == scoreDto.A &&
                B == scoreDto.B;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, matchId, A, B);
    }
}
