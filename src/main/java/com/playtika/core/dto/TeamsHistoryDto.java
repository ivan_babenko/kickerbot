package com.playtika.core.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class TeamsHistoryDto {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int matchId;

    private long ts = System.currentTimeMillis();

    private int playerA1;
    private int playerA2;
    private int playerB1;
    private int playerB2;

    public TeamsHistoryDto() {
    }

    public TeamsHistoryDto(int playerA1, int playerA2, int playerB1, int playerB2) {
        this.playerA1 = playerA1;
        this.playerA2 = playerA2;
        this.playerB1 = playerB1;
        this.playerB2 = playerB2;
    }

    @Override
    public String toString() {
        return "TeamsHistoryDto{" +
                "matchId=" + matchId +
                ", ts=" + ts +
                ", playerA1=" + playerA1 +
                ", playerA2=" + playerA2 +
                ", playerB1=" + playerB1 +
                ", playerB2=" + playerB2 +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TeamsHistoryDto that = (TeamsHistoryDto) o;
        return matchId == that.matchId &&
                ts == that.ts &&
                playerA1 == that.playerA1 &&
                playerA2 == that.playerA2 &&
                playerB1 == that.playerB1 &&
                playerB2 == that.playerB2;
    }

    @Override
    public int hashCode() {
        return Objects.hash(matchId, ts, playerA1, playerA2, playerB1, playerB2);
    }

    public long getTs() {
        return ts;
    }

    public void setTs(long ts) {
        this.ts = ts;
    }

    public int getMatchId() {
        return matchId;
    }

    public void setMatchId(int matchId) {
        this.matchId = matchId;
    }

    public int getPlayerA1() {
        return playerA1;
    }

    public void setPlayerA1(int playerA1) {
        this.playerA1 = playerA1;
    }

    public int getPlayerA2() {
        return playerA2;
    }

    public void setPlayerA2(int playerA2) {
        this.playerA2 = playerA2;
    }

    public int getPlayerB1() {
        return playerB1;
    }

    public void setPlayerB1(int playerB1) {
        this.playerB1 = playerB1;
    }

    public int getPlayerB2() {
        return playerB2;
    }

    public void setPlayerB2(int playerB2) {
        this.playerB2 = playerB2;
    }
}
