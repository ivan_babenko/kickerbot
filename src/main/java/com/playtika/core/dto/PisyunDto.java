package com.playtika.core.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class PisyunDto {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private int matchId;
    private long ts;
    private int userId;


    public PisyunDto() {
    }

    public PisyunDto(int matchId, long ts, int userId) {
        this.matchId = matchId;
        this.ts = ts;
        this.userId = userId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PisyunDto{");
        sb.append("id=").append(id);
        sb.append(", matchId=").append(matchId);
        sb.append(", ts=").append(ts);
        sb.append(", userId=").append(userId);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PisyunDto pisyunDto = (PisyunDto) o;
        return id == pisyunDto.id &&
                matchId == pisyunDto.matchId &&
                ts == pisyunDto.ts &&
                userId == pisyunDto.userId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, matchId, ts, userId);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMatchId() {
        return matchId;
    }

    public void setMatchId(int matchId) {
        this.matchId = matchId;
    }

    public long getTs() {
        return ts;
    }

    public void setTs(long ts) {
        this.ts = ts;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
