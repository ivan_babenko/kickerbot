package com.playtika.core.dto;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class UserStatsDto {

    @Id
    private int userId;
    private boolean isPisyun;
    private long pisyunStartTime;
    private int totalWins;
    private int totalLosses;


    public UserStatsDto() {
    }

    public UserStatsDto(int userId, boolean isPisyun, long pisyunStartTime, int totalWins, int totalLosses) {
        this.userId = userId;
        this.isPisyun = isPisyun;
        this.pisyunStartTime = pisyunStartTime;
        this.totalWins = totalWins;
        this.totalLosses = totalLosses;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public boolean isPisyun() {
        return isPisyun;
    }

    public void setPisyun(boolean pisyun) {
        isPisyun = pisyun;
    }

    public long getPisyunStartTime() {
        return pisyunStartTime;
    }

    public void setPisyunStartTime(long pisyunStartTime) {
        this.pisyunStartTime = pisyunStartTime;
    }

    public int getTotalWins() {
        return totalWins;
    }

    public void setTotalWins(int totalWins) {
        this.totalWins = totalWins;
    }

    public int getTotalLosses() {
        return totalLosses;
    }

    public void setTotalLosses(int totalLosses) {
        this.totalLosses = totalLosses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserStatsDto that = (UserStatsDto) o;
        return userId == that.userId &&
                isPisyun == that.isPisyun &&
                pisyunStartTime == that.pisyunStartTime &&
                totalWins == that.totalWins &&
                totalLosses == that.totalLosses;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, isPisyun, pisyunStartTime, totalWins, totalLosses);
    }

    @Override
    public String toString() {
        return "UserStatsDto{" +
                "userId=" + userId +
                ", isPisyun=" + isPisyun +
                ", pisyunStartTime=" + pisyunStartTime +
                ", totalWins=" + totalWins +
                ", totalLosses=" + totalLosses +
                '}';
    }
}
