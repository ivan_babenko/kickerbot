package com.playtika.core;

import com.playtika.core.dto.UserDto;
import org.telegram.telegrambots.api.objects.User;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

public class Utils {

    public static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    public static final long ONE_DAY = 86_400_000;

    public static String formatExpirationTime(long ts) {
        return formatter.format(LocalDateTime.ofInstant(
                Instant.ofEpochMilli(ts),
                TimeZone.getDefault().toZoneId()));
    }

    public static String makeName(User user) {
        StringBuilder sb = new StringBuilder();
        if (user.getFirstName() != null) {
            sb.append(user.getFirstName());
            sb.append(" ");
        }
        if (user.getLastName() != null) {
            sb.append(user.getLastName());
        }
        return sb.toString();
    }

    public static String makeName(UserDto user) {
        StringBuilder sb = new StringBuilder();
        if (user.getFirstName() != null) {
            sb.append(user.getFirstName());
            sb.append(" ");
        }
        if (user.getLastName() != null) {
            sb.append(user.getLastName());
        }
        return sb.toString();
    }


    public static String getTeamNames(List<User> team) {
        return team
                .stream()
                .map(Utils::makeName)
                .collect(Collectors.joining("\n"));
    }


    public static long getDaysInMilliseconds(Matcher matcher, int windowDays, int groupId) {
        int multiplier = windowDays;
        if (matcher.find()) {
            String daysToCount = matcher.group(groupId);
            if (daysToCount != null) {
                multiplier = Integer.valueOf(daysToCount);
            }
        }
        return multiplier * Utils.ONE_DAY;
    }

}
