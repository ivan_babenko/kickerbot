package com.playtika.core

import org.json.JSONObject
import org.junit.Ignore
import org.telegram.telegrambots.api.objects.User

class UserPoolTest extends GroovyTestCase {

    @Ignore("NO TESTS!")
    void testSplitPool() {
        UserPool pool = new UserPool()

        pool.addUserToPool(newUser("a", "b", "c", 1))
        pool.addUserToPool(newUser("d", "e", "f", 2))
        pool.addUserToPool(newUser("g", "h", "i", 3))
        pool.addUserToPool(newUser("j", "k", "l", 4))

        def teamA = [];
        def teamB = [];
        pool.splitPool(teamA, teamB)

        assertEquals(2, teamA.size())
        assertEquals(2, teamB.size())
        teamA.each {
            assertFalse(teamB.contains(it))
        }
        teamB.each {
            assertFalse(teamA.contains(it))
        }
    }

    private User newUser(fName, lName, uName, id) {
        new User(new JSONObject(
                first_name: fName,
                last_name: lName,
                user_name: uName,
                id: id
        ))
    }


}
